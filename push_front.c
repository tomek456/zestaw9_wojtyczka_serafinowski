#include "moduly.h"

void push_front(struct Client **head, char *p_surname);

// Add element to the beginning of a list (new head)
void push_front(struct Client **head, char *p_surname)
{
    struct Client *new;
    new = (struct Client*)malloc(sizeof(struct Client));

    strcpy(new->surname, p_surname);
    *head = new; // new element is a head now

    return;
}
