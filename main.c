#include "moduly.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{

	struct Client *head;

		char surname[100]="";
		int opcja=0;

        head=(struct Client*)malloc(sizeof(struct Client));
        head=0;


        //Tutaj mamy glowna czesc programu

        while(opcja!=5)
		{
        //Wybieramy z bazy dostepnch opcji cyfre od 1-5
        printf("1 - Dodaj klienta. \n");
        printf("2 - Usun klienta. \n");
        printf("3 - Wyswietl ilone klientow. \n");
        printf("4 - Wyswietl nazwiska klientow. \n");
        printf("5 - Zakoncz. \n");
        scanf("%d", &opcja);

     	switch(opcja)
            {
            case 1:
                nowy(&head);
                break;
            case 2:
                printf("Wybierz nazwisko, ktore chcesz usunac: \n");
                scanf("%s",surname);
                pop_by_surname(&head,surname);
                break;
            case 3:
                list_size(head);
                break;
            case 4:
                show_list(head);
                break;
            case 5:
                exit(1);
             default:
                exit(1);
            }
        }

return 0;
}
