#include "moduly.h"

void show_list(struct Client *head);

void show_list(struct Client *head)
{
    if(head == NULL)
        printf("Empty list\n");
    else
    {
        printf("\nList of clients: \n");
        struct Client *somebody = head;
        do
        {
            printf("%s\n", somebody->surname);
            somebody = somebody->next;

        } while(somebody != NULL);
    }

    return;
}
