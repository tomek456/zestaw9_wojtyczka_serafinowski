#include "moduly.h"

void pop_by_surname(struct Client **head, char *p_surname);

// Delete first element of given name
void pop_by_surname(struct Client **head, char *p_surname)
{
    struct Client *var, *previous;
    var=*head;
    char surnameremove[100];
    printf("Wprowadz nazwisko ktore chcesz usunac. \n");
    scanf("%s", surnameremove);
    while(var->next!=0)
    {
        if(strcmp(surnameremove, var->surname)==0)
        {
            break;
        }
        previous=var;
        var=var->next;
    }
    if (var!=0)
    {
        if (var!=*head )
        {
            previous->next=var->next;
        }
        else
        {
            *head=var->next;
        }
        free(var);
        printf("Nazwisko usuniete. \n");
    }
    return;
}
