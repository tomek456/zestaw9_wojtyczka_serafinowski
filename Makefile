CC=gcc
CLAGS=-Wall
LIBS=-lm

main: main.o list_size.o nowy_klient.o pop_back.o pop_by_index.o pop_by_surname.o pop_front.o push_back.o push_by_index.o push_front.o show_list.o
	$(CC) $(CFLAGS) -o main main.o pop_back.o list_size.o nowy_klient.o pop_by_index.o pop_by_surname.o pop_front.o push_back.o push_by_index.o push_front.o show_list.o $(LIBS)

main.o: main.c moduly.h
	$(CC) $(CFLAGS) -c main.c

pop_back.o: pop_back.c 
	$(CC) $(CFLAGS) -c pop_back.c 

list_size.o: list_size.c
	$(CC) $(CFLAGS) -c list_size.c

nowy_klient.o: nowy_klient.c
	$(CC) $(CFLAGS) -c nowy_klient.c

pop_by_index.o: pop_by_index.c
	$(CC) $(CFLAGS) -c pop_by_index.c

pop_by_surname.o: pop_by_surname.c
	$(CC) $(CFLAGS) -c pop_by_surname.c
	
pop_front.o: pop_front.c
	$(CC) $(CFLAGS) -c pop_front.c

push_back.o: push_back.c
	$(CC) $(CFLAGS) -c push_back.c
	
push_by_index.o: push_by_index.c
	$(CC) $(CFLAGS) -c push_by_index.c
	
push_front.o: push_front.c
	$(CC) $(CFLAGS) -c push_front.c
	
show_list.o: show_list.c
	$(CC) $(CFLAGS) -c show_list.c